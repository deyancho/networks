/**
 * Created by deyan on 10.02.17.
 */

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

public class Receiver2a {

    public static void main(String [] args) throws IOException, InterruptedException {

        System.out.println("Format - java Receiver2a <port> <filePath to save>");

        int port = Integer.parseInt(args[0]);
        String fileReceivedPath = args[1];
        ArrayList<Byte> bytesReceived = new ArrayList<Byte>();

        DatagramSocket serverSocket = new DatagramSocket(port);
        byte[] receiveData = new byte[1027];

        int lastAck = -1000; // just some random initial value
        System.out.println("Waiting for file reception...");
        int expectedSeqNum = 1;
        int initAckNum = 0;
        byte[] ack = new byte[2];
        ack[0] = (byte) ((initAckNum >> 8) & 0xff);
        //ack[1] = (byte)(initAckNum >> 8);
        //ack[0] = (byte)(initAckNum);
        ack[1] = (byte)(initAckNum & 0xff);

        System.out.println("Made initial ACK " + (((ack[0] & 0xff) << 8) + (ack[1] & 0xff)));

        while (true) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);

            byte [] data = new byte[receivePacket.getLength()];
            System.arraycopy(receivePacket.getData(), receivePacket.getOffset(), data, 0, receivePacket.getLength());

            //int currentPacket = ((segmentByteArray[0] & 0xff) << 8) + (segmentByteArray[1] & 0xff);
            int senderPort = receivePacket.getPort();
            InetAddress senderAddress = receivePacket.getAddress();
            //int packetReceivedLbl = ((data[1] & 0xff) << 8) + (data[0] & 0xff);
            int packetReceivedLbl = ((data[0] & 0xff) << 8) + (data[1] & 0xff);
            System.out.println("Received " + packetReceivedLbl);

//            System.out.println("Received binaries from sender.");
//            System.out.println(Integer.toBinaryString((data[0] & 0xFF) + 0x100).substring(1));
//            System.out.println(Integer.toBinaryString((data[1] & 0xFF) + 0x100).substring(1));

            if (packetReceivedLbl == expectedSeqNum) {

                for (int j = 3; j<data.length;j++) {
                    bytesReceived.add(data[j]);
                }

                ack[0] = ((byte)((packetReceivedLbl >> 8) & 0xff));
                ack[1] = ((byte)(packetReceivedLbl & 0xff));

                DatagramPacket acknowledgement = new  DatagramPacket(ack, ack.length, senderAddress, senderPort);
                serverSocket.send(acknowledgement);
                int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);
                System.out.println("Packet received - sending ACK with label " + send);

//                System.out.println(Integer.toBinaryString((ack[0] & 0xFF) + 0x100).substring(1));
//                System.out.println(Integer.toBinaryString((ack[1] & 0xFF) + 0x100).substring(1));

                expectedSeqNum += 1;
                if (data[2]==1) {
                    lastAck = send;
                    break;
                };
            }

            else {

                DatagramPacket acknowledgement = new  DatagramPacket(ack, ack.length, senderAddress, senderPort);
                int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);
                serverSocket.send(acknowledgement);
                System.out.println("Packet already received - dropping and sending ACK with label " + send);
//                System.out.println(Integer.toBinaryString((ack[0] & 0xFF) + 0x100).substring(1));
//                System.out.println(Integer.toBinaryString((ack[1] & 0xFF) + 0x100).substring(1));

            }
        }

        byte[] byteArrayReceivedImg = new byte[bytesReceived.size()];
        for(int l = 0; l < bytesReceived.size(); l++) {
            byteArrayReceivedImg[l] = bytesReceived.get(l);
        }

        OutputStream out = new FileOutputStream(fileReceivedPath);
        out.write(byteArrayReceivedImg);
        out.flush();
        out.close();

        System.out.println("File received. Socket remaining open in case of last ack lost.");

        while (true) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);

            int senderPort = receivePacket.getPort();
            InetAddress senderAddress = receivePacket.getAddress();

            ack[0] = ((byte)((lastAck >> 8) & 0xff));
            ack[1] = ((byte)(lastAck & 0xff));

            DatagramPacket acknowledgement = new  DatagramPacket(ack, ack.length, senderAddress, senderPort);
            serverSocket.send(acknowledgement);
            int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);
            System.out.println("Packet received - sending last ACK with label " + send);
        }
    }

}
