/**
 * Created by deyan on 10.02.17.
 */

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

public class Receiver2b {

    public static void main(String [] args) throws IOException, InterruptedException {

        System.out.println("Format - java Receiver2b <port> <filePath to save> <windowSize>");

        int port = Integer.parseInt(args[0]);
        String fileReceivedPath = args[1];
        int windowSize = Integer.parseInt(args[2]);

        int lastAckNum = -1000;   // just some random initial value
        boolean fileReceived = false;

        int base = 1;

        HashMap<Integer,ArrayList<Byte>> messagesStorage = new HashMap<Integer,ArrayList<Byte>>();

        ArrayList<Byte> bytesReceived = new ArrayList<Byte>();

        DatagramSocket serverSocket = new DatagramSocket(port);
        byte[] receiveData = new byte[1027];

        System.out.println("Waiting for file reception...");

        byte[] ack = new byte[2];

        while (!fileReceived) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);

            byte [] data = new byte[receivePacket.getLength()];
            System.arraycopy(receivePacket.getData(), receivePacket.getOffset(), data, 0, receivePacket.getLength());

            int senderPort = receivePacket.getPort();
            InetAddress senderAddress = receivePacket.getAddress();
            int packetReceivedLbl = ((data[0] & 0xff) << 8) + (data[1] & 0xff);
            System.out.println("Received packet " + packetReceivedLbl);

            if (data[2] == 1) {
                lastAckNum = packetReceivedLbl;
            }

            if (base-windowSize <= packetReceivedLbl && packetReceivedLbl < base) {

                ack[0] = ((byte)((packetReceivedLbl >> 8) & 0xff));
                ack[1] = ((byte)(packetReceivedLbl & 0xff));

                DatagramPacket acknowledgement = new  DatagramPacket(ack, ack.length, senderAddress, senderPort);
                serverSocket.send(acknowledgement);
                int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);
                System.out.println("Resending ACK with label " + send + " below base");

            }

            else if (base <= packetReceivedLbl && packetReceivedLbl < base + windowSize) {

                ack[0] = ((byte)((packetReceivedLbl >> 8) & 0xff));
                ack[1] = ((byte)(packetReceivedLbl & 0xff));

                DatagramPacket acknowledgement = new DatagramPacket(ack, ack.length, senderAddress, senderPort);
                serverSocket.send(acknowledgement);
                int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);

                if (messagesStorage.get(packetReceivedLbl)==null) {
                    System.out.println("Storing packet and sending ACK in window with label " + send);
                    ArrayList<Byte> bytesPacket = new ArrayList<Byte>();
                    for (int j = 3; j < data.length; j++) {
                        bytesPacket.add(data[j]);
                    }

                    messagesStorage.put(packetReceivedLbl, bytesPacket);

                    for (int i = base; i <= base + windowSize; i++) {
                        if (messagesStorage.get(i)==null) {
                            base = i;
                            break;
                        }
                    }

                }
                else {
                    System.out.println("Packet already stored - resending ACK in window with label " + send);
                }
            }
            else {
                // do nothing
                System.out.println("Packet " + packetReceivedLbl + " is after window. This is an error. Base is " + base);
            }

            if (base == lastAckNum + 1) {
                fileReceived = true;
            }

        }

        int numPackets = messagesStorage.size();
        for (int j = 0; j< numPackets;j++) {
            for (int k = 0; k < messagesStorage.get(j+1).size();k++) {
                bytesReceived.add(messagesStorage.get(j+1).get(k));
            }
        }

        byte[] byteArrayReceivedImg = new byte[bytesReceived.size()];
        for(int l = 0; l < bytesReceived.size(); l++) {
            byteArrayReceivedImg[l] = bytesReceived.get(l);
        }

        OutputStream out = new FileOutputStream(fileReceivedPath);
        out.write(byteArrayReceivedImg);
        out.flush();
        out.close();
	
	int timeToWaitAfterAckBeforeClosing = 10000;

	int timeToWaitSecs = timeToWaitAfterAckBeforeClosing/1000;

        System.out.println("File received. Socket remaining open for " + timeToWaitSecs +  " seconds in case of lost acks.");
	
	 // in milliseconds
        while (true) {

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.setSoTimeout(timeToWaitAfterAckBeforeClosing);

            try {
                serverSocket.receive(receivePacket);
            }catch (IOException e) {
                break;
            }

            byte [] data = new byte[receivePacket.getLength()];
            System.arraycopy(receivePacket.getData(), receivePacket.getOffset(), data, 0, receivePacket.getLength());

            int senderPort = receivePacket.getPort();
            InetAddress senderAddress = receivePacket.getAddress();
            int packetReceivedLbl = ((data[0] & 0xff) << 8) + (data[1] & 0xff);
            System.out.println("Received packet " + packetReceivedLbl);

            ack[0] = ((byte)((packetReceivedLbl >> 8) & 0xff));
            ack[1] = ((byte)(packetReceivedLbl & 0xff));

            DatagramPacket acknowledgement = new  DatagramPacket(ack, ack.length, senderAddress, senderPort);
            serverSocket.send(acknowledgement);
            int send = ((ack[0] & 0xff) << 8) + (ack[1] & 0xff);
            System.out.println("Resending ACK with label " + send);

        }
	System.out.println(timeToWaitSecs + " seconds has passed after sending an ACK. Closing socket.");

    }

}
