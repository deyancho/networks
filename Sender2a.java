import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Sender2a {

    public static volatile int base = 1;    // base modified by thread for timer - the latest acknowledged packet

    public static volatile int nextSeqNum = 1;  // not modified but must be accessed by timer thread

    public static volatile boolean timeout = false;   // has a timeout occurred for the current timer?

    public static volatile boolean setTimer = true;  // setTimer allows to set a timer for only the earliest unacked packet(base)

    public static volatile boolean lastAckReceived = false;   // have we received ack for the end packet?

    public static byte[] inputToBArray(BufferedInputStream in){

        /**
         * Converts BufferedInputStream to byte array.
         */

        try {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                bos.write(buf, 0, len);
            }
            byte[] b=bos.toByteArray();

            return b;
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }

        byte [] bla = new byte[0];
        return bla;


    }

    public static void main(String args[]) {

        try {

            System.out.println("Format - java Sender2a localhost <port receiver> <filepath> <retryTimeout> [window]");

            BufferedInputStream is = new BufferedInputStream(new FileInputStream(args[2]));     // save file as BufferedInputStream

            byte[] bytes = inputToBArray(is);

            int packetSize = 1024;

            DatagramSocket senderSocket = new DatagramSocket();

            byte endOfFile = 0;  // are we sending the ending packet?

            double packetsToSend = (Math.ceil((float) bytes.length / packetSize)); // number of packets to send.

            System.out.println("Sending file via packets...");

            int window = Integer.parseInt(args[4]);

            int portReceiver = Integer.parseInt(args[1]);       // port of the receiver

            InetAddress address = InetAddress.getByName(args[0]); // the internet address of the localhost

            int retryTimeout = Integer.parseInt(args[3]);

            Timer t1 = new Timer(senderSocket, retryTimeout, packetsToSend);   // start receiver thread
            t1.start();

            HashMap<Integer,DatagramPacket> messagesStorage = new HashMap<Integer,DatagramPacket>();    // buffer for sent messages

            int numberRetransmissions = 0;

            long startTime = System.currentTimeMillis();

            do  {

                if (nextSeqNum < base + window && nextSeqNum <= packetsToSend) {	// if pipeline is not full, fill it up

                    ArrayList<Byte> segmentArrayLst = new ArrayList<Byte>();
                    segmentArrayLst.add((byte) ((nextSeqNum >> 8) & 0xff));
                    segmentArrayLst.add((byte) (nextSeqNum & 0xff));

                    if (nextSeqNum == packetsToSend) {
                        endOfFile = 1;
                    }

                    segmentArrayLst.add(endOfFile);

                    for (int j = 0; j < packetSize; j++) {      // if we're at the last byte, put byte in arraylist and break
                        if (j + (nextSeqNum - 1) * 1024 == bytes.length - 1) {
                            segmentArrayLst.add(bytes[j + (nextSeqNum - 1) * 1024]);
                            break;
                        }
                        segmentArrayLst.add(bytes[j + (nextSeqNum - 1) * 1024]);
                    }

                    int n = segmentArrayLst.size();
                    byte[] segmentByteArray = new byte[n];
                    for (int k = 0; k < n; k++) {         // convert from arraylist to byte array for sending over udp.
                        segmentByteArray[k] = segmentArrayLst.get(k);
                    }

                    DatagramPacket segmentSend = new DatagramPacket(segmentByteArray, segmentByteArray.length, address, portReceiver);

                    senderSocket.send(segmentSend);	// send the message
                    int currentPacket = ((segmentByteArray[0] & 0xff) << 8) + (segmentByteArray[1] & 0xff);
                    System.out.println("Sent packet with label " + currentPacket);

//                    System.out.println(Integer.toBinaryString((segmentByteArray[0] & 0xFF) + 0x100).substring(1));
//                    System.out.println(Integer.toBinaryString((segmentByteArray[1] & 0xFF) + 0x100).substring(1));

                    messagesStorage.put(nextSeqNum, segmentSend); // put it in the buffer for potential resend

                    if (base == nextSeqNum && setTimer) {	// if base == nextSeqNum set the timer for the first message (the earliest unacked) only
                        setTimer = false;	// don't set timers for consecutive messages
                        timeout = false;	// no timeout has occurred yet
                        Timer.timeWaited = 0;	// reset the timer
                    }

                    nextSeqNum += 1;
                }

                if (timeout) {		// if a timeout has occurred, resend messages numbered from the earliest unacked message to the nextSeqNum
                    timeout = false;
                    setTimer = false;
                    //Timer.timeWaited = 0; // reset the timer for the first resent message

                    for (int message = base; message <= nextSeqNum-1; message++) {
                        senderSocket.send(messagesStorage.get(message));
                        numberRetransmissions+=1;
                        System.out.println("Resent packet " + message);
                    }
                }

            } while (!lastAckReceived);

            System.out.println("Done.");

            long estimatedTime = System.currentTimeMillis() - startTime;

            double elapsedTimeSeconds =  (estimatedTime / 1000.0);
            double fileKB = (double)((bytes.length) / 1024.0);
            double throughput = (double) fileKB / elapsedTimeSeconds;

            System.out.println("");
            System.out.println("Stats:");
            System.out.println("Number of retransmited packets: " + numberRetransmissions);
            System.out.println("Throughput: " + throughput + " KB/s.");
            return;

        } catch (Exception e) {
            System.out.println(e.toString());
            System.exit(1);

        }

    }
}


class Timer extends Thread
{
    DatagramSocket socket;
    int timeoutLength;
    double packetsToSend;
    public static volatile int timeWaited = 0;

    public Timer(DatagramSocket socket, int timeoutLength, double packetsToSend)
    {
        this.socket = socket;
        this.timeoutLength = timeoutLength;
        this.packetsToSend = packetsToSend;
    }

    @Override
    public void run()
    {
        while (!Sender2a.lastAckReceived) {	// receive acks until no more left
            timeWaited = 0;
            while (timeWaited < timeoutLength) {	// wait for acks for a time of max timeoutLength
                try{

                    byte[] receiveData = new byte[2];
                    DatagramPacket ack = new DatagramPacket(receiveData, receiveData.length);
                    long startWait = System.currentTimeMillis();	// start waiting for an ack
                    socket.setSoTimeout(timeoutLength - timeWaited);
                    socket.receive(ack);

                    long endWait = System.currentTimeMillis();		// end waiting for an ack

                    int timeChunk = (int) (endWait - startWait);	// how long did we wait from this iteration?
                    timeWaited += timeChunk;				// how long have we waited in total?

                    int labelAck = ((receiveData[0] & 0xff) << 8) + (receiveData[1] & 0xff);

                    if (labelAck == packetsToSend) {
                        Sender2a.lastAckReceived = true;
                    }

                    System.out.println("Received ACK with label " + labelAck);
//                    System.out.println(Integer.toBinaryString((receiveData[0] & 0xFF) + 0x100).substring(1));
//                    System.out.println(Integer.toBinaryString((receiveData[1] & 0xFF) + 0x100).substring(1));

                    Sender2a.base = labelAck + 1;

                    if (Sender2a.base == Sender2a.nextSeqNum) {		// if the sender base number == sender nextSeqNum, allow him to set a timer again
                        Sender2a.timeout = false;
                        Sender2a.setTimer = true;
                        break;
                    }

                    if (Sender2a.lastAckReceived){
                        return;
                    }

                }catch (IOException e) {

                    if (Sender2a.lastAckReceived){
                        return;
                    }

                    Sender2a.timeout = true;
                    System.out.println("Timeout on packet " + Sender2a.base);

                }
            }
        }
        return;
    }

}
