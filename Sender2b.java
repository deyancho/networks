import java.io.*;
import java.net.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.HashMap;
import java.util.ArrayList;

public class Sender2b {

    public static volatile int base = 1;    // base modified by thread for receiver

    public static volatile int nextSeqNum = 1;

    public static volatile boolean fileSent = false;

    public static volatile int windowSize = -15;  // just initialize with something

    public static volatile Timer timer = new Timer(true);

    public static volatile int retryTimeout = -15; // just initialize with something

    public static volatile int numberRetransmissions = 0;

    public static volatile DatagramSocket socket;

    public static HashMap<Integer,DatagramPacket> messagesStorage = new HashMap<Integer,DatagramPacket>();     // we'll store the whole file here - overhead but whatever

    public static volatile ArrayList<Integer> unackedPackets = new ArrayList<Integer>();     // sequence numbers of all unacked packets

    public static byte[] inputToBArray(BufferedInputStream in){

        /**
         * Converts BufferedInputStream to byte array.
         */

        try {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                bos.write(buf, 0, len);
            }
            byte[] b=bos.toByteArray();

            return b;
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }

        byte [] byteArr = new byte[0];
        return byteArr;
    }

    // Handles the timeouts

    public static class Timeout extends TimerTask {

        int packetNumber;

        public Timeout(int packetNumber) {
            this.packetNumber = packetNumber;

        }
        public void run() {

            // if the packet has been received before the timeout occurs, do nothing
            if (unackedPackets.contains(packetNumber)) {
                try {
                    socket.send(messagesStorage.get(packetNumber));
                    System.out.println("Timeout - resent " + packetNumber);
                    numberRetransmissions++;
                    timer.schedule(new Timeout(packetNumber), retryTimeout);
                    Thread.sleep(1);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                return;
            }
        }
    }

    public static class Receiver implements Runnable
    {
        DatagramSocket socket;

        public Receiver(DatagramSocket socket)

        {
            this.socket = socket;
        }

        @Override
        public void run()
        {
            while (!Sender2b.fileSent) {

                try{

                    byte[] receiveData = new byte[2];
                    DatagramPacket ack = new DatagramPacket(receiveData, receiveData.length);
                    socket.receive(ack);

                    int labelAck = ((receiveData[0] & 0xff) << 8) + (receiveData[1] & 0xff);

                    if (unackedPackets.contains(labelAck)) {
                        int indexOfElement = unackedPackets.indexOf(labelAck);
                        unackedPackets.remove(indexOfElement);
                        if (unackedPackets.size() == 0) {
                            fileSent = true;
                        }
                    }

                    for (int i = base; i <= base + windowSize; i++) {
                        if (unackedPackets.contains(i)) {
                            base = i;
                            break;
                        }
                    }

                    System.out.println("Received ACK with label " + labelAck);

                }catch (IOException e) {

                }
            }
            return;
        }

    }

    public static void main(String args[]) {

        try {

            System.out.println("Format - java Sender2b localhost <port receiver> <filepath> <retryTimeout> [window]");

            BufferedInputStream is = new BufferedInputStream(new FileInputStream(args[2]));     // save file as BufferedInputStream

            byte[] bytes = inputToBArray(is);

            int packetSize = 1024;

            socket = new DatagramSocket();

            double packetsToSend = (Math.ceil((float) bytes.length / packetSize)); // number of packets to send.

            windowSize = Integer.parseInt(args[4]);

            int portReceiver = Integer.parseInt(args[1]);       // port of the receiver

            InetAddress address = InetAddress.getByName(args[0]); // the internet address of the localhost

            retryTimeout = Integer.parseInt(args[3]);

            for (int i = 1; i<=packetsToSend; i++) {

                byte thirdByte;
                unackedPackets.add(i);
                ArrayList<Byte> packetToStore = new ArrayList<Byte>();
                packetToStore.add((byte) ((i >> 8) & 0xff));
                packetToStore.add((byte) (i & 0xff));

                if (i == packetsToSend) {
                    thirdByte = 1;
                }
                else {
                    thirdByte = 0;
                }
                packetToStore.add(thirdByte);

                for (int j = 0; j < packetSize; j++) {      // if we're at the last byte, put byte in arraylist and break
                    if (j + (i - 1) * 1024 == bytes.length - 1) {
                        packetToStore.add(bytes[j + (i - 1) * 1024]);
                        break;
                    }
                    packetToStore.add(bytes[j + (i - 1) * 1024]);
                }

                int n0 = packetToStore.size();
                byte[] bytesToStore = new byte[n0];
                for (int k0 = 0; k0 < n0; k0++) {         // convert from arraylist to byte array for sending over udp.
                    bytesToStore[k0] = packetToStore.get(k0);
                }

                DatagramPacket segmentToStore = new DatagramPacket(bytesToStore, bytesToStore.length, address, portReceiver);

                messagesStorage.put(i,segmentToStore);
            }

            System.out.println("Sending file via packets...");

            Receiver t1 = new Receiver(socket);   // start receiver thread
            Thread threadRecv = new Thread(t1);
            threadRecv.start();


            long startTime = System.currentTimeMillis();


            do  {

                if (nextSeqNum < base + windowSize && nextSeqNum <= packetsToSend) {	// if pipeline is not full, fill it up

                    socket.send(messagesStorage.get(nextSeqNum));
                    System.out.println("Sent packet numbered " + nextSeqNum);
                    timer.schedule(new Timeout(nextSeqNum), retryTimeout);
                    Thread.sleep(1);    // do this because the loop is very fast - if we don't wait at all the timertask scheduled may be out of order, i.e. resent packet 4, resent packet 1
                    nextSeqNum += 1;

                }

            } while (!fileSent);

            System.out.println("Done. File sent.");

            long estimatedTime = System.currentTimeMillis() - startTime;

            double elapsedTimeSeconds =  (estimatedTime / 1000.0);
            double fileKB = (double)((bytes.length) / 1024.0);
            double throughput = (double) fileKB / elapsedTimeSeconds;

            System.out.println("");
            System.out.println("Stats:");
            System.out.println("Number of retransmited packets: " + numberRetransmissions);
            System.out.println("Throughput: " + throughput + " KB/s.");
            return;

        } catch (Exception e) {
            System.out.println(e.toString());
            System.exit(1);

        }

    }
}

